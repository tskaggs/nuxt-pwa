module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Load Test',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },
  /*
  ** Customize the base url
  */
  router: {
    base: '/nuxt-pwa/'
  },
  /*
  ** Nuxt PWA
  */
  modules: [
    '@nuxtjs/pwa'
  ],
  manifest: {
    name: 'Load Test',
    lang: 'en'
  },
  workbox: {
    dev: true,
    importScripts: [
      'custom-sw.js'
    ],
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
